-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2023. Okt 04. 13:47
-- Kiszolgáló verziója: 10.4.27-MariaDB
-- PHP verzió: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `lionsdale`
--
CREATE DATABASE IF NOT EXISTS `lionsdale` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
USE `lionsdale`;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `nationality_id` int(11) NOT NULL,
  `postal_code` varchar(12) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `addresses`
--

INSERT INTO `addresses` (`id`, `nationality_id`, `postal_code`, `street`) VALUES
(11, 10, '1234', 'asdasawfawfa'),
(18, 101, '1234', 'Petőfi');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `explosives`
--

CREATE TABLE `explosives` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qty_name` varchar(50) NOT NULL,
  `price` double(11,2) NOT NULL,
  `description` varchar(255) DEFAULT 'Goes bumm!',
  `picture` varchar(255) NOT NULL DEFAULT 'tnt.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `explosives`
--

INSERT INTO `explosives` (`id`, `name`, `qty_name`, `price`, `description`, `picture`) VALUES
(1, 'TNT', 'Köteg', 25.00, 'Goes bumm!', 'tnt.png'),
(2, 'C4', 'KG', 100.00, 'Don\'t blow up yourself.', 'tnt.png'),
(3, 'Grenade', 'Piece', 75.00, 'Throw in or throw up.', 'tnt.png'),
(4, 'PipeBomb', 'Pipe', 80.00, 'It fits everywhere', 'tnt.png'),
(5, 'Incendinary', 'Piece', 125.00, 'Let all hell loose.', 'tnt.png');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- A tábla adatainak kiíratása `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`) VALUES
(1, 'af', 'Afrikaans'),
(2, 'sq', 'Albanian - shqip'),
(3, 'am', 'Amharic - አማርኛ'),
(4, 'ar', 'Arabic - العربية'),
(5, 'an', 'Aragonese - aragonés'),
(6, 'hy', 'Armenian - հայերեն'),
(7, 'ast', 'Asturian - asturianu'),
(8, 'az', 'Azerbaijani - azərbaycan dili'),
(9, 'eu', 'Basque - euskara'),
(10, 'be', 'Belarusian - беларуская'),
(11, 'bn', 'Bengali - বাংলা'),
(12, 'bs', 'Bosnian - bosanski'),
(13, 'br', 'Breton - brezhoneg'),
(14, 'bg', 'Bulgarian - български'),
(15, 'ca', 'Catalan - català'),
(16, 'ckb', 'Central Kurdish - کوردی (دەستنوسی عەرەبی)'),
(17, 'zh', 'Chinese - 中文'),
(18, 'zh-HK', 'Chinese (Hong Kong) - 中文（香港）'),
(19, 'zh-CN', 'Chinese (Simplified) - 中文（简体）'),
(20, 'zh-TW', 'Chinese (Traditional) - 中文（繁體）'),
(21, 'co', 'Corsican'),
(22, 'hr', 'Croatian - hrvatski'),
(23, 'cs', 'Czech - čeština'),
(24, 'da', 'Danish - dansk'),
(25, 'nl', 'Dutch - Nederlands'),
(26, 'en', 'English'),
(27, 'en-AU', 'English (Australia)'),
(28, 'en-CA', 'English (Canada)'),
(29, 'en-IN', 'English (India)'),
(30, 'en-NZ', 'English (New Zealand)'),
(31, 'en-ZA', 'English (South Africa)'),
(32, 'en-GB', 'English (United Kingdom)'),
(33, 'en-US', 'English (United States)'),
(34, 'eo', 'Esperanto - esperanto'),
(35, 'et', 'Estonian - eesti'),
(36, 'fo', 'Faroese - føroyskt'),
(37, 'fil', 'Filipino'),
(38, 'fi', 'Finnish - suomi'),
(39, 'fr', 'French - français'),
(40, 'fr-CA', 'French (Canada) - français (Canada)'),
(41, 'fr-FR', 'French (France) - français (France)'),
(42, 'fr-CH', 'French (Switzerland) - français (Suisse)'),
(43, 'gl', 'Galician - galego'),
(44, 'ka', 'Georgian - ქართული'),
(45, 'de', 'German - Deutsch'),
(46, 'de-AT', 'German (Austria) - Deutsch (Österreich)'),
(47, 'de-DE', 'German (Germany) - Deutsch (Deutschland)'),
(48, 'de-LI', 'German (Liechtenstein) - Deutsch (Liechtenstein)'),
(49, 'de-CH', 'German (Switzerland) - Deutsch (Schweiz)'),
(50, 'el', 'Greek - Ελληνικά'),
(51, 'gn', 'Guarani'),
(52, 'gu', 'Gujarati - ગુજરાતી'),
(53, 'ha', 'Hausa'),
(54, 'haw', 'Hawaiian - ʻŌlelo Hawaiʻi'),
(55, 'he', 'Hebrew - עברית'),
(56, 'hi', 'Hindi - हिन्दी'),
(57, 'hu', 'Hungarian - magyar'),
(58, 'is', 'Icelandic - íslenska'),
(59, 'id', 'Indonesian - Indonesia'),
(60, 'ia', 'Interlingua'),
(61, 'ga', 'Irish - Gaeilge'),
(62, 'it', 'Italian - italiano'),
(63, 'it-IT', 'Italian (Italy) - italiano (Italia)'),
(64, 'it-CH', 'Italian (Switzerland) - italiano (Svizzera)'),
(65, 'ja', 'Japanese - 日本語'),
(66, 'kn', 'Kannada - ಕನ್ನಡ'),
(67, 'kk', 'Kazakh - қазақ тілі'),
(68, 'km', 'Khmer - ខ្មែរ'),
(69, 'ko', 'Korean - 한국어'),
(70, 'ku', 'Kurdish - Kurdî'),
(71, 'ky', 'Kyrgyz - кыргызча'),
(72, 'lo', 'Lao - ລາວ'),
(73, 'la', 'Latin'),
(74, 'lv', 'Latvian - latviešu'),
(75, 'ln', 'Lingala - lingála'),
(76, 'lt', 'Lithuanian - lietuvių'),
(77, 'mk', 'Macedonian - македонски'),
(78, 'ms', 'Malay - Bahasa Melayu'),
(79, 'ml', 'Malayalam - മലയാളം'),
(80, 'mt', 'Maltese - Malti'),
(81, 'mr', 'Marathi - मराठी'),
(82, 'mn', 'Mongolian - монгол'),
(83, 'ne', 'Nepali - नेपाली'),
(84, 'no', 'Norwegian - norsk'),
(85, 'nb', 'Norwegian Bokmål - norsk bokmål'),
(86, 'nn', 'Norwegian Nynorsk - nynorsk'),
(87, 'oc', 'Occitan'),
(88, 'or', 'Oriya - ଓଡ଼ିଆ'),
(89, 'om', 'Oromo - Oromoo'),
(90, 'ps', 'Pashto - پښتو'),
(91, 'fa', 'Persian - فارسی'),
(92, 'pl', 'Polish - polski'),
(93, 'pt', 'Portuguese - português'),
(94, 'pt-BR', 'Portuguese (Brazil) - português (Brasil)'),
(95, 'pt-PT', 'Portuguese (Portugal) - português (Portugal)'),
(96, 'pa', 'Punjabi - ਪੰਜਾਬੀ'),
(97, 'qu', 'Quechua'),
(98, 'ro', 'Romanian - română'),
(99, 'mo', 'Romanian (Moldova) - română (Moldova)'),
(100, 'rm', 'Romansh - rumantsch'),
(101, 'ru', 'Russian - русский'),
(102, 'gd', 'Scottish Gaelic'),
(103, 'sr', 'Serbian - српски'),
(104, 'sh', 'Serbo-Croatian - Srpskohrvatski'),
(105, 'sn', 'Shona - chiShona'),
(106, 'sd', 'Sindhi'),
(107, 'si', 'Sinhala - සිංහල'),
(108, 'sk', 'Slovak - slovenčina'),
(109, 'sl', 'Slovenian - slovenščina'),
(110, 'so', 'Somali - Soomaali'),
(111, 'st', 'Southern Sotho'),
(112, 'es', 'Spanish - español'),
(113, 'es-AR', 'Spanish (Argentina) - español (Argentina)'),
(114, 'es-419', 'Spanish (Latin America) - español (Latinoamérica)'),
(115, 'es-MX', 'Spanish (Mexico) - español (México)'),
(116, 'es-ES', 'Spanish (Spain) - español (España)'),
(117, 'es-US', 'Spanish (United States) - español (Estados Unidos)'),
(118, 'su', 'Sundanese'),
(119, 'sw', 'Swahili - Kiswahili'),
(120, 'sv', 'Swedish - svenska'),
(121, 'tg', 'Tajik - тоҷикӣ'),
(122, 'ta', 'Tamil - தமிழ்'),
(123, 'tt', 'Tatar'),
(124, 'te', 'Telugu - తెలుగు'),
(125, 'th', 'Thai - ไทย'),
(126, 'ti', 'Tigrinya - ትግርኛ'),
(127, 'to', 'Tongan - lea fakatonga'),
(128, 'tr', 'Turkish - Türkçe'),
(129, 'tk', 'Turkmen'),
(130, 'tw', 'Twi'),
(131, 'uk', 'Ukrainian - українська'),
(132, 'ur', 'Urdu - اردو'),
(133, 'ug', 'Uyghur'),
(134, 'uz', 'Uzbek - o‘zbek'),
(135, 'vi', 'Vietnamese - Tiếng Việt'),
(136, 'wa', 'Walloon - wa'),
(137, 'cy', 'Welsh - Cymraeg'),
(138, 'fy', 'Western Frisian'),
(139, 'xh', 'Xhosa'),
(140, 'yi', 'Yiddish'),
(141, 'yo', 'Yoruba - Èdè Yorùbá'),
(142, 'zu', 'Zulu - isiZulu');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(11) NOT NULL,
  `code` char(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone_prefix` int(5) NOT NULL,
  `currency_symbol` varchar(10) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `nationalities`
--

INSERT INTO `nationalities` (`id`, `code`, `name`, `phone_prefix`, `currency_symbol`, `currency`) VALUES
(1, 'AF', 'Afghanistan', 93, '؋', 'AFN'),
(2, 'AX', 'Aland Islands', 358, '€', 'EUR'),
(3, 'AL', 'Albania', 355, 'Lek', 'ALL'),
(4, 'DZ', 'Algeria', 213, 'دج', 'DZD'),
(5, 'AS', 'American Samoa', 1684, '$', 'USD'),
(6, 'AD', 'Andorra', 376, '€', 'EUR'),
(7, 'AO', 'Angola', 244, 'Kz', 'AOA'),
(8, 'AI', 'Anguilla', 1264, '$', 'XCD'),
(9, 'AQ', 'Antarctica', 672, '$', 'AAD'),
(10, 'AG', 'Antigua and Barbuda', 1268, '$', 'XCD'),
(11, 'AR', 'Argentina', 54, '$', 'ARS'),
(12, 'AM', 'Armenia', 374, '֏', 'AMD'),
(13, 'AW', 'Aruba', 297, 'ƒ', 'AWG'),
(14, 'AU', 'Australia', 61, '$', 'AUD'),
(15, 'AT', 'Austria', 43, '€', 'EUR'),
(16, 'AZ', 'Azerbaijan', 994, 'm', 'AZN'),
(17, 'BS', 'Bahamas', 1242, 'B$', 'BSD'),
(18, 'BH', 'Bahrain', 973, '.د.ب', 'BHD'),
(19, 'BD', 'Bangladesh', 880, '৳', 'BDT'),
(20, 'BB', 'Barbados', 1246, 'Bds$', 'BBD'),
(21, 'BY', 'Belarus', 375, 'Br', 'BYN'),
(22, 'BE', 'Belgium', 32, '€', 'EUR'),
(23, 'BZ', 'Belize', 501, '$', 'BZD'),
(24, 'BJ', 'Benin', 229, 'CFA', 'XOF'),
(25, 'BM', 'Bermuda', 1441, '$', 'BMD'),
(26, 'BT', 'Bhutan', 975, 'Nu.', 'BTN'),
(27, 'BO', 'Bolivia', 591, 'Bs.', 'BOB'),
(28, 'BQ', 'Bonaire, Sint Eustatius and Saba', 599, '$', 'USD'),
(29, 'BA', 'Bosnia and Herzegovina', 387, 'KM', 'BAM'),
(30, 'BW', 'Botswana', 267, 'P', 'BWP'),
(31, 'BV', 'Bouvet Island', 55, 'kr', 'NOK'),
(32, 'BR', 'Brazil', 55, 'R$', 'BRL'),
(33, 'IO', 'British Indian Ocean Territory', 246, '$', 'USD'),
(34, 'BN', 'Brunei Darussalam', 673, 'B$', 'BND'),
(35, 'BG', 'Bulgaria', 359, 'Лв.', 'BGN'),
(36, 'BF', 'Burkina Faso', 226, 'CFA', 'XOF'),
(37, 'BI', 'Burundi', 257, 'FBu', 'BIF'),
(38, 'KH', 'Cambodia', 855, 'KHR', 'KHR'),
(39, 'CM', 'Cameroon', 237, 'FCFA', 'XAF'),
(40, 'CA', 'Canada', 1, '$', 'CAD'),
(41, 'CV', 'Cape Verde', 238, '$', 'CVE'),
(42, 'KY', 'Cayman Islands', 1345, '$', 'KYD'),
(43, 'CF', 'Central African Republic', 236, 'FCFA', 'XAF'),
(44, 'TD', 'Chad', 235, 'FCFA', 'XAF'),
(45, 'CL', 'Chile', 56, '$', 'CLP'),
(46, 'CN', 'China', 86, '¥', 'CNY'),
(47, 'CX', 'Christmas Island', 61, '$', 'AUD'),
(48, 'CC', 'Cocos (Keeling) Islands', 672, '$', 'AUD'),
(49, 'CO', 'Colombia', 57, '$', 'COP'),
(50, 'KM', 'Comoros', 269, 'CF', 'KMF'),
(51, 'CG', 'Congo', 242, 'FC', 'XAF'),
(52, 'CD', 'Congo, Democratic Republic of the Congo', 242, 'FC', 'CDF'),
(53, 'CK', 'Cook Islands', 682, '$', 'NZD'),
(54, 'CR', 'Costa Rica', 506, '₡', 'CRC'),
(55, 'CI', 'Cote D\'Ivoire', 225, 'CFA', 'XOF'),
(56, 'HR', 'Croatia', 385, 'kn', 'HRK'),
(57, 'CU', 'Cuba', 53, '$', 'CUP'),
(58, 'CW', 'Curacao', 599, 'ƒ', 'ANG'),
(59, 'CY', 'Cyprus', 357, '€', 'EUR'),
(60, 'CZ', 'Czech Republic', 420, 'Kč', 'CZK'),
(61, 'DK', 'Denmark', 45, 'Kr.', 'DKK'),
(62, 'DJ', 'Djibouti', 253, 'Fdj', 'DJF'),
(63, 'DM', 'Dominica', 1767, '$', 'XCD'),
(64, 'DO', 'Dominican Republic', 1809, '$', 'DOP'),
(65, 'EC', 'Ecuador', 593, '$', 'USD'),
(66, 'EG', 'Egypt', 20, 'ج.م', 'EGP'),
(67, 'SV', 'El Salvador', 503, '$', 'USD'),
(68, 'GQ', 'Equatorial Guinea', 240, 'FCFA', 'XAF'),
(69, 'ER', 'Eritrea', 291, 'Nfk', 'ERN'),
(70, 'EE', 'Estonia', 372, '€', 'EUR'),
(71, 'ET', 'Ethiopia', 251, 'Nkf', 'ETB'),
(72, 'FK', 'Falkland Islands (Malvinas)', 500, '£', 'FKP'),
(73, 'FO', 'Faroe Islands', 298, 'Kr.', 'DKK'),
(74, 'FJ', 'Fiji', 679, 'FJ$', 'FJD'),
(75, 'FI', 'Finland', 358, '€', 'EUR'),
(76, 'FR', 'France', 33, '€', 'EUR'),
(77, 'GF', 'French Guiana', 594, '€', 'EUR'),
(78, 'PF', 'French Polynesia', 689, '₣', 'XPF'),
(79, 'TF', 'French Southern Territories', 262, '€', 'EUR'),
(80, 'GA', 'Gabon', 241, 'FCFA', 'XAF'),
(81, 'GM', 'Gambia', 220, 'D', 'GMD'),
(82, 'GE', 'Georgia', 995, 'ლ', 'GEL'),
(83, 'DE', 'Germany', 49, '€', 'EUR'),
(84, 'GH', 'Ghana', 233, 'GH₵', 'GHS'),
(85, 'GI', 'Gibraltar', 350, '£', 'GIP'),
(86, 'GR', 'Greece', 30, '€', 'EUR'),
(87, 'GL', 'Greenland', 299, 'Kr.', 'DKK'),
(88, 'GD', 'Grenada', 1473, '$', 'XCD'),
(89, 'GP', 'Guadeloupe', 590, '€', 'EUR'),
(90, 'GU', 'Guam', 1671, '$', 'USD'),
(91, 'GT', 'Guatemala', 502, 'Q', 'GTQ'),
(92, 'GG', 'Guernsey', 44, '£', 'GBP'),
(93, 'GN', 'Guinea', 224, 'FG', 'GNF'),
(94, 'GW', 'Guinea-Bissau', 245, 'CFA', 'XOF'),
(95, 'GY', 'Guyana', 592, '$', 'GYD'),
(96, 'HT', 'Haiti', 509, 'G', 'HTG'),
(97, 'HM', 'Heard Island and McDonald Islands', 0, '$', 'AUD'),
(98, 'VA', 'Holy See (Vatican City State)', 39, '€', 'EUR'),
(99, 'HN', 'Honduras', 504, 'L', 'HNL'),
(100, 'HK', 'Hong Kong', 852, '$', 'HKD'),
(101, 'HU', 'Hungary', 36, 'Ft', 'HUF'),
(102, 'IS', 'Iceland', 354, 'kr', 'ISK'),
(103, 'IN', 'India', 91, '₹', 'INR'),
(104, 'ID', 'Indonesia', 62, 'Rp', 'IDR'),
(105, 'IR', 'Iran, Islamic Republic of', 98, '﷼', 'IRR'),
(106, 'IQ', 'Iraq', 964, 'د.ع', 'IQD'),
(107, 'IE', 'Ireland', 353, '€', 'EUR'),
(108, 'IM', 'Isle of Man', 44, '£', 'GBP'),
(109, 'IL', 'Israel', 972, '₪', 'ILS'),
(110, 'IT', 'Italy', 39, '€', 'EUR'),
(111, 'JM', 'Jamaica', 1876, 'J$', 'JMD'),
(112, 'JP', 'Japan', 81, '¥', 'JPY'),
(113, 'JE', 'Jersey', 44, '£', 'GBP'),
(114, 'JO', 'Jordan', 962, 'ا.د', 'JOD'),
(115, 'KZ', 'Kazakhstan', 7, 'лв', 'KZT'),
(116, 'KE', 'Kenya', 254, 'KSh', 'KES'),
(117, 'KI', 'Kiribati', 686, '$', 'AUD'),
(118, 'KP', 'Korea, Democratic People\'s Republic of', 850, '₩', 'KPW'),
(119, 'KR', 'Korea, Republic of', 82, '₩', 'KRW'),
(120, 'XK', 'Kosovo', 383, '€', 'EUR'),
(121, 'KW', 'Kuwait', 965, 'ك.د', 'KWD'),
(122, 'KG', 'Kyrgyzstan', 996, 'лв', 'KGS'),
(123, 'LA', 'Lao People\'s Democratic Republic', 856, '₭', 'LAK'),
(124, 'LV', 'Latvia', 371, '€', 'EUR'),
(125, 'LB', 'Lebanon', 961, '£', 'LBP'),
(126, 'LS', 'Lesotho', 266, 'L', 'LSL'),
(127, 'LR', 'Liberia', 231, '$', 'LRD'),
(128, 'LY', 'Libyan Arab Jamahiriya', 218, 'د.ل', 'LYD'),
(129, 'LI', 'Liechtenstein', 423, 'CHf', 'CHF'),
(130, 'LT', 'Lithuania', 370, '€', 'EUR'),
(131, 'LU', 'Luxembourg', 352, '€', 'EUR'),
(132, 'MO', 'Macao', 853, '$', 'MOP'),
(133, 'MK', 'Macedonia, the Former Yugoslav Republic of', 389, 'ден', 'MKD'),
(134, 'MG', 'Madagascar', 261, 'Ar', 'MGA'),
(135, 'MW', 'Malawi', 265, 'MK', 'MWK'),
(136, 'MY', 'Malaysia', 60, 'RM', 'MYR'),
(137, 'MV', 'Maldives', 960, 'Rf', 'MVR'),
(138, 'ML', 'Mali', 223, 'CFA', 'XOF'),
(139, 'MT', 'Malta', 356, '€', 'EUR'),
(140, 'MH', 'Marshall Islands', 692, '$', 'USD'),
(141, 'MQ', 'Martinique', 596, '€', 'EUR'),
(142, 'MR', 'Mauritania', 222, 'MRU', 'MRO'),
(143, 'MU', 'Mauritius', 230, '₨', 'MUR'),
(144, 'YT', 'Mayotte', 262, '€', 'EUR'),
(145, 'MX', 'Mexico', 52, '$', 'MXN'),
(146, 'FM', 'Micronesia, Federated States of', 691, '$', 'USD'),
(147, 'MD', 'Moldova, Republic of', 373, 'L', 'MDL'),
(148, 'MC', 'Monaco', 377, '€', 'EUR'),
(149, 'MN', 'Mongolia', 976, '₮', 'MNT'),
(150, 'ME', 'Montenegro', 382, '€', 'EUR'),
(151, 'MS', 'Montserrat', 1664, '$', 'XCD'),
(152, 'MA', 'Morocco', 212, 'DH', 'MAD'),
(153, 'MZ', 'Mozambique', 258, 'MT', 'MZN'),
(154, 'MM', 'Myanmar', 95, 'K', 'MMK'),
(155, 'NA', 'Namibia', 264, '$', 'NAD'),
(156, 'NR', 'Nauru', 674, '$', 'AUD'),
(157, 'NP', 'Nepal', 977, '₨', 'NPR'),
(158, 'NL', 'Netherlands', 31, '€', 'EUR'),
(159, 'AN', 'Netherlands Antilles', 599, 'NAf', 'ANG'),
(160, 'NC', 'New Caledonia', 687, '₣', 'XPF'),
(161, 'NZ', 'New Zealand', 64, '$', 'NZD'),
(162, 'NI', 'Nicaragua', 505, 'C$', 'NIO'),
(163, 'NE', 'Niger', 227, 'CFA', 'XOF'),
(164, 'NG', 'Nigeria', 234, '₦', 'NGN'),
(165, 'NU', 'Niue', 683, '$', 'NZD'),
(166, 'NF', 'Norfolk Island', 672, '$', 'AUD'),
(167, 'MP', 'Northern Mariana Islands', 1670, '$', 'USD'),
(168, 'NO', 'Norway', 47, 'kr', 'NOK'),
(169, 'OM', 'Oman', 968, '.ع.ر', 'OMR'),
(170, 'PK', 'Pakistan', 92, '₨', 'PKR'),
(171, 'PW', 'Palau', 680, '$', 'USD'),
(172, 'PS', 'Palestinian Territory, Occupied', 970, '₪', 'ILS'),
(173, 'PA', 'Panama', 507, 'B/.', 'PAB'),
(174, 'PG', 'Papua New Guinea', 675, 'K', 'PGK'),
(175, 'PY', 'Paraguay', 595, '₲', 'PYG'),
(176, 'PE', 'Peru', 51, 'S/.', 'PEN'),
(177, 'PH', 'Philippines', 63, '₱', 'PHP'),
(178, 'PN', 'Pitcairn', 64, '$', 'NZD'),
(179, 'PL', 'Poland', 48, 'zł', 'PLN'),
(180, 'PT', 'Portugal', 351, '€', 'EUR'),
(181, 'PR', 'Puerto Rico', 1787, '$', 'USD'),
(182, 'QA', 'Qatar', 974, 'ق.ر', 'QAR'),
(183, 'RE', 'Reunion', 262, '€', 'EUR'),
(184, 'RO', 'Romania', 40, 'lei', 'RON'),
(185, 'RU', 'Russian Federation', 7, '₽', 'RUB'),
(186, 'RW', 'Rwanda', 250, 'FRw', 'RWF'),
(187, 'BL', 'Saint Barthelemy', 590, '€', 'EUR'),
(188, 'SH', 'Saint Helena', 290, '£', 'SHP'),
(189, 'KN', 'Saint Kitts and Nevis', 1869, '$', 'XCD'),
(190, 'LC', 'Saint Lucia', 1758, '$', 'XCD'),
(191, 'MF', 'Saint Martin', 590, '€', 'EUR'),
(192, 'PM', 'Saint Pierre and Miquelon', 508, '€', 'EUR'),
(193, 'VC', 'Saint Vincent and the Grenadines', 1784, '$', 'XCD'),
(194, 'WS', 'Samoa', 684, 'SAT', 'WST'),
(195, 'SM', 'San Marino', 378, '€', 'EUR'),
(196, 'ST', 'Sao Tome and Principe', 239, 'Db', 'STD'),
(197, 'SA', 'Saudi Arabia', 966, '﷼', 'SAR'),
(198, 'SN', 'Senegal', 221, 'CFA', 'XOF'),
(199, 'RS', 'Serbia', 381, 'din', 'RSD'),
(200, 'CS', 'Serbia and Montenegro', 381, 'din', 'RSD'),
(201, 'SC', 'Seychelles', 248, 'SRe', 'SCR'),
(202, 'SL', 'Sierra Leone', 232, 'Le', 'SLL'),
(203, 'SG', 'Singapore', 65, '$', 'SGD'),
(204, 'SX', 'St Martin', 721, 'ƒ', 'ANG'),
(205, 'SK', 'Slovakia', 421, '€', 'EUR'),
(206, 'SI', 'Slovenia', 386, '€', 'EUR'),
(207, 'SB', 'Solomon Islands', 677, 'Si$', 'SBD'),
(208, 'SO', 'Somalia', 252, 'Sh.so.', 'SOS'),
(209, 'ZA', 'South Africa', 27, 'R', 'ZAR'),
(210, 'GS', 'South Georgia and the South Sandwich Islands', 500, '£', 'GBP'),
(211, 'SS', 'South Sudan', 211, '£', 'SSP'),
(212, 'ES', 'Spain', 34, '€', 'EUR'),
(213, 'LK', 'Sri Lanka', 94, 'Rs', 'LKR'),
(214, 'SD', 'Sudan', 249, '.س.ج', 'SDG'),
(215, 'SR', 'Suriname', 597, '$', 'SRD'),
(216, 'SJ', 'Svalbard and Jan Mayen', 47, 'kr', 'NOK'),
(217, 'SZ', 'Swaziland', 268, 'E', 'SZL'),
(218, 'SE', 'Sweden', 46, 'kr', 'SEK'),
(219, 'CH', 'Switzerland', 41, 'CHf', 'CHF'),
(220, 'SY', 'Syrian Arab Republic', 963, 'LS', 'SYP'),
(221, 'TW', 'Taiwan, Province of China', 886, '$', 'TWD'),
(222, 'TJ', 'Tajikistan', 992, 'SM', 'TJS'),
(223, 'TZ', 'Tanzania, United Republic of', 255, 'TSh', 'TZS'),
(224, 'TH', 'Thailand', 66, '฿', 'THB'),
(225, 'TL', 'Timor-Leste', 670, '$', 'USD'),
(226, 'TG', 'Togo', 228, 'CFA', 'XOF'),
(227, 'TK', 'Tokelau', 690, '$', 'NZD'),
(228, 'TO', 'Tonga', 676, '$', 'TOP'),
(229, 'TT', 'Trinidad and Tobago', 1868, '$', 'TTD'),
(230, 'TN', 'Tunisia', 216, 'ت.د', 'TND'),
(231, 'TR', 'Turkey', 90, '₺', 'TRY'),
(232, 'TM', 'Turkmenistan', 7370, 'T', 'TMT'),
(233, 'TC', 'Turks and Caicos Islands', 1649, '$', 'USD'),
(234, 'TV', 'Tuvalu', 688, '$', 'AUD'),
(235, 'UG', 'Uganda', 256, 'USh', 'UGX'),
(236, 'UA', 'Ukraine', 380, '₴', 'UAH'),
(237, 'AE', 'United Arab Emirates', 971, 'إ.د', 'AED'),
(238, 'GB', 'United Kingdom', 44, '£', 'GBP'),
(239, 'US', 'United States', 1, '$', 'USD'),
(240, 'UM', 'United States Minor Outlying Islands', 1, '$', 'USD'),
(241, 'UY', 'Uruguay', 598, '$', 'UYU'),
(242, 'UZ', 'Uzbekistan', 998, 'лв', 'UZS'),
(243, 'VU', 'Vanuatu', 678, 'VT', 'VUV'),
(244, 'VE', 'Venezuela', 58, 'Bs', 'VEF'),
(245, 'VN', 'Viet Nam', 84, '₫', 'VND'),
(246, 'VG', 'Virgin Islands, British', 1284, '$', 'USD'),
(247, 'VI', 'Virgin Islands, U.s.', 1340, '$', 'USD'),
(248, 'WF', 'Wallis and Futuna', 681, '₣', 'XPF'),
(249, 'EH', 'Western Sahara', 212, 'MAD', 'MAD'),
(250, 'YE', 'Yemen', 967, '﷼', 'YER'),
(251, 'ZM', 'Zambia', 260, 'ZK', 'ZMW'),
(252, 'ZW', 'Zimbabwe', 263, '$', 'ZWL');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `passwords`
--

CREATE TABLE `passwords` (
  `id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `passwords`
--

INSERT INTO `passwords` (`id`, `password`) VALUES
(14, '$2y$10$xN5ZrscC.qBRt5xq6DCi3OTkafS3X7HbGcDTGpEDf1QWa/t2qEyB.'),
(17, '$2y$10$Z3KvMyhambksVs6APcUE/OCThrzXwLsdYr0ftg8CS2puTk7xvIVIW'),
(18, '$2y$10$xPfaStzoI.vK9bKdTT20mOBR.CkzIIoqGXWyN/SSjz4XoylDiQRy.'),
(19, '$2y$10$mX5Q40AOW.POYtGdGlr7mOIvgd7QQzEZbiK87dca4LWvn2aIIhQka'),
(20, '$2y$10$kyWRl/2TnJ.VQBsedmjWDuAmJY5jpXYRu7nKoAuB2NE1AWRbgpj1u'),
(21, '$2y$10$Mq4NTwdLbt6mg17tWy4z6OwLLLgvzp/VuT0sPptdsTmGhC6Ncd/4m'),
(22, '$2y$10$Bt8LlZ1oarkmaJp4Jmgyu.Gkl4mCP39/jGDYIUoNG3I2hu4rCYToe'),
(23, '$2y$10$4PdfDHjSHV0v9C/M36f1guSSnwbWMNGNzZpjGxMW1HcyAjNhuhPG2'),
(24, '$2y$10$36dLZWwqzNE1Nyiz4oH4WORHFojH3extMUH8TzN5crJC00BwIAYMG'),
(25, '$2y$10$vwnkIdyrX88fQ.rkqPi5veUYhcskEaoyl3Kep/qncQts6/AZhzsD.'),
(26, '$2y$10$rAO3bzLkT9ZiWTfDIl86mOKxYCeu9VyDLPYpJktO1eKtDPdx.V.CK'),
(27, '$2y$10$ijOw8OsbINVlF8377IMP4.faupZ8Y3jiOaBz/0Eb3qkUr4Y0LPUh.'),
(28, '$2y$10$8nR27Y7SlQeZ5frKdoZS9OpDKGuXZxixalqDOLUnjzn63dGbTDy2i'),
(29, '$2y$10$mCSmHcDEmNXjRFXknIf8huJk7TDBCZ0hai6q09jEdw3OfWDZ6lIPC'),
(30, '$2y$10$.NpopptT3BtSRLs6T0n9D..c5PQYiFLwbRiIg6OD5NffwJD1Sq9QS'),
(31, '$2y$10$yIBb/xT4yKexUTCimx/TmeGhlWQD8Yzvr49emcIGFrb.Jz3gy7W7u'),
(32, '$2y$10$wOWoCnsYWuWG3YBoW6yWveJAVXhJsYDQErVuuyrzXaXd2NSg8eEjW'),
(33, '$2y$10$lqkXhPJdw3jyktOSP4vtmOsjlkygNYCzOD2UDFNoc715AjZMu6FHi'),
(34, '$2y$10$nEndHrpzQX8mlfCUZgfEM.ZHnUX85A1xY27pb/ZlYVCEwzLfhKOqm'),
(35, '$2y$10$vxdkZ5oj1FrXl5qieWMTG.ClWqUnpceYGv/rccrKGx3LjWw9AJl5m'),
(36, '$2y$10$9uhCpFoRu5W1WBCN9gzO5OOgZGQHEMCWgFTQw57c/s5kAe892kOjK');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password_id` int(11) NOT NULL,
  `birthdate` date NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address_id` int(11) NOT NULL,
  `omid` varchar(20) DEFAULT NULL,
  `nationality_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `sex_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password_id`, `birthdate`, `phone`, `email`, `address_id`, `omid`, `nationality_id`, `language_id`, `sex_id`) VALUES
(33, 'Rick Wick', 'Rick', 36, '1981-01-12', '(90) 123-4567', 'richardwyeth@gmail.com', 18, 'Nincs', 101, 54, 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nationality_id` (`nationality_id`);

--
-- A tábla indexei `explosives`
--
ALTER TABLE `explosives`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `passwords`
--
ALTER TABLE `passwords`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `omid` (`omid`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `nationality_id` (`nationality_id`),
  ADD KEY `password_id` (`password_id`),
  ADD KEY `address_id` (`address_id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT a táblához `explosives`
--
ALTER TABLE `explosives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT a táblához `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT a táblához `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT a táblához `passwords`
--
ALTER TABLE `passwords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`id`);

--
-- Megkötések a táblához `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`password_id`) REFERENCES `passwords` (`id`),
  ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
