<?php 

if (isset($_POST['submit'])) {
    session_start();

    /* Kosár lékérése a sessionből, és adatok átkérése POST metódussal */
    $cart = $_SESSION['cart'];
    $update_id = $_POST['update_id'];
    $qty = $_POST['qty'];

    /* Kosárban lévő termék mennyiségének változtatása */
    /* Mennyiség Nagyobb mint nulla és egész szám*/
    /* Ha a lefelé kerekített szám egyenlő a kapott számmal 
    (pl.3.2 kerekítve 3 nem egyenlő 3.2-vel*/
    if ($qty > 0 && floor($qty) == $qty) { 
        /* Átállítom a mennyiséget */
        $cart[$update_id]['qty'] = $qty;
        $_SESSION['cart'] = $cart;
        header("Location: ../views/tnt.php?update=success");
    }
    else if ($qty <= 0 && floor($qty) == $qty){
        /* Törlöm a terméket */
        unset($cart[$update_id]);
        $_SESSION['cart'] = $cart;
        header("Location: ../views/tnt.php?update=success");
    }
    else {
        echo "Something's wrong in the neighbourhood. Who you gonna call?";
    }
}