<?php
session_start();

/* Kosárba rakás */
if (isset($_POST['submit'])) {
    require_once 'database.php';

    $cart;

    /* Ha van már a kosárban valami (vagy létre lett hozva már korábban) */
    /* Akkor lementem */
    /* Ha nincs, akkor csak csinálok egy üres tömböt. */
    if (isset($_SESSION['cart'])) {
        $cart = $_SESSION['cart'];
    } else {
        $cart = array();
    }


    /* Injection védelem */
    $id = mysqli_real_escape_string($connection, $_POST['id']);
    $quantity = mysqli_real_escape_string($connection, $_POST['quantity']);


    /* Az átadott id alapján a termék lekérése */
    $query = "SELECT * FROM explosives WHERE id = $id";
    $result = mysqli_query($connection, $query);

    /* Megvan a termék (ha nem egy eredményt kapunk, gáz van) */
    if (mysqli_num_rows($result) == 1) {
        /* Asszociatív tömbbe való mentés */
        $product = mysqli_fetch_assoc($result);

        /* Annak ellenőrzése, hogy a termék már szerepel-e a kosárban */
        $vanAkosarban = false;

        /* Végigmegyek a kosárban szereplő termékeken */
        /* Ha az adatbázisból (imént kosárba rakott) lekért termék ID egyezést mutat valamelyik IDVEL */
        /* Akkor csak hozzáadom a qty-t */
        foreach ($cart as $key => $value) {
            if ($product['id'] == $value['id']) {
                $cart[$key]['qty'] += $quantity;
                $vanAkosarban = true;
                break;
            }
        }
        if ($vanAkosarban == false) {
            /* Mennyiség hozzáadása a producthoz */
            $product["qty"] = $quantity;
            /* A termék hozzáadása a kosárhoz */
            array_push($cart, $product);
        }


        /* A sessionben található kosár tömb felülírása, a most megcsinált kosárral. */
        $_SESSION['cart'] = $cart;
        header("Location: ../views/tnt.php");
    } else {
        header("Location: ../views/tnt.php?error=producterror");
    }
} else {
    header("Location: ../views/tnt.php?error=cheat");

}
