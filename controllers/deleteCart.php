<?php

if (isset($_POST['submit'])) {

    session_start();

    $cart = $_SESSION['cart'];
    $delete_id = $_POST['delete_id'];

    unset($cart[$delete_id]);

    $_SESSION['cart'] = $cart;

    header("Location: ../views/tnt.php?remove=success");
}
else {
    header("Location: ../views/tnt.php?error=dontCheatMf");
}