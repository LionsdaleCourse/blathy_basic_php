<?php

if (isset($_POST['submit'])) {
    session_start();
    require_once 'database.php';

    if (isset($_SESSION['cart'])) {
        $cart = $_SESSION['cart'];
    } else {
        die('Nincs kosár');
    }

    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $fullName = mysqli_real_escape_string($connection, $_POST['fullname'][0]);
    $postal = mysqli_real_escape_string($connection, $_POST['postal'][0]);
    $country = mysqli_real_escape_string($connection, $_POST['country'][0]);
    $street = mysqli_real_escape_string($connection, $_POST['street'][0]);
    $phone = mysqli_real_escape_string($connection, $_POST['phone']);

    /* Ugyanaz-e a számlázás mint a szállítás vagy sem */
    if (isset($_POST['switch'])) {
        /* Ugyanaz */
        $del_fullName = $fullName;
        $del_postal = $postal;
        $del_country = $country;
        $del_street = $street;
    } else {
        /* Különbözik */
        $del_fullName = mysqli_real_escape_string($connection, $_POST['fullname'][1]);
        $del_postal = mysqli_real_escape_string($connection, $_POST['postal'][1]);
        $del_country = mysqli_real_escape_string($connection, $_POST['country'][1]);
        $del_street = mysqli_real_escape_string($connection, $_POST['street'][1]);
    }

    /* Van-e loginolt user? */

    if (isset($_SESSION['user'])) {
        $user_id = $_SESSION['user_id'];
    }
    else {
        $user_id = 0;
    }

    /* Adatok feltöltése az orders táblába */
    $sql_query = "INSERT INTO `orders`(`user_id`, `email`, `fullName`, `postal`,
     `country_id`, `street`, `phone`, `del_name`, `del_postal`,
      `del_country_id`, `del_street`) 
    VALUES ($user_id, '$email', '$fullName', '$postal', $country, '$street', 
    '$phone', '$del_fullName', '$del_postal', $del_country, '$del_street')";

    mysqli_query($connection, $sql_query);

    /* Adatok feltöltése az explosive_order táblába */
    
    /* Legutolsó beillesztett sor ID lekérése */
    $last_id = mysqli_insert_id($connection);

    foreach ($cart as $key => $value) {
        $exp_id = $value['id'];
        $qty = $value['qty'];
        $price = $value['price'];
        
        $ex_query = "INSERT INTO `explosive_order`(`explosive_id`, `order_id`, `qty`, `egysegar`) 
    VALUES ($exp_id, $last_id, $qty, $price)";

    mysqli_query($connection, $ex_query);

    echo "Sikeresen bement a". $value['name'];
    }

    /* Kosár törlése */
    $_SESSION['cart'] = null;

    


}