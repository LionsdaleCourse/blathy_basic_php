<?php require_once "../layouts/header.php"; ?>
<?php require_once '../controllers/database.php'; ?>

<?php
/* TODO: Complete validation rules on inputs after password */
/* Nationality lekérése */
/* 1. Csatlakozok az adatbázishoz és lefutattom az SQL kódot */
/* 2. Lement az eredményt egy változóba */
/* 3. Kiiratom az eredeményt */

$sql_query = "SELECT * FROM nationalities";
$result = mysqli_query($connection, $sql_query);

$sql_query1 = "SELECT * FROM nationalities";
$result1 = mysqli_query($connection, $sql_query1);

$sql_query2 = "SELECT * FROM languages";
$result2 = mysqli_query($connection, $sql_query2);
?>

<!-- Content eleje -->
<div class="container mt-5">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="row text-center">
                        <h4 class="card-title">Registration</h4>
                    </div>

                    <form action="../controllers/registration.php" method="POST" id="registrationForm">
                        <!-- Method lehet GET vagy POST -->
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" name="email" required>
                        </div>
                        <div class="passwordParent mb-3">
                            <label for="exampleInputPassword1" class="form-label">Password</label>
                            <span class="passwordIconHolder">
                                <i class="fa-solid fa-eye passwordIcon"
                                    onclick="passwordVisibility('exampleInputPassword1', this)"></i>
                            </span>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="password"
                                minlength="8"
                                data-parsley-pattern="^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$">
                        </div>
                        <div class="passwordParent mb-3">
                            <label for="exampleInputPassword1" class="form-label">Password confirm</label>
                            <input type="password" class="form-control" id="exampleInputPassword2"
                                name="passwordConfirm" data-parsley-equalto="#exampleInputPassword1">
                            <span class="passwordIconHolder">
                                <i class="fa-solid fa-eye passwordIcon"
                                    onclick="passwordVisibility('exampleInputPassword2', this)"></i>
                            </span>
                        </div>
                        <div class="mb-3">
                            <label for="Username" class="form-label">Username</label>
                            <input minlength="4" type="text" class="form-control" id="Username" name="username">
                        </div>
                        <div class="mb-3">
                            <label for="Birthdate" class="form-label">Birthdate</label>
                            <input type="date" class="form-control" id="Birthdate" name="birthdate">
                        </div>
                        <div class="mb-3">
                            <label for="FullName" class="form-label">FullName</label>
                            <input type="text" class="form-control" id="FullName" name="fullname">
                        </div>
                        <div class="mb-3">
                            <label for="postal" class="form-label">Postal Code</label>
                            <input type="number" class="form-control" id="postal" name="postal">
                        </div>
                        <div class="mb-3">
                            <label for="Country" class="form-label">Country</label>
                            <select id="Country" class="form-control" name="country">
                                <option value="#" disabled selected>Please choose</option>

                                <?php
                                while ($row = mysqli_fetch_assoc($result)) {
                                    echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="Street" class="form-label">Street</label>
                            <input type="text" class="form-control" id="Street" name="street">
                        </div>
                        <div class="mb-3">
                            <label for="omid" class="form-label">OM ID</label>
                            <input type="text" class="form-control" id="omid" name="omid">
                        </div>
                        <div class="mb-3">
                            <label for="Nationality" class="form-label">Nationality</label>
                            <select id="Nationality" class="form-control" name="nationality">
                                <option value="#" disabled selected>Please choose</option>

                                <?php
                                while ($row = mysqli_fetch_assoc($result1)) {
                                    echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="Language" class="form-label">Language</label>
                            <select id="Language" class="form-control" name="language">
                                <option value="#" disabled selected>Please choose</option>

                                <?php
                                while ($row = mysqli_fetch_assoc($result2)) {
                                    echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="mb-3 form-group">
                            <label class="form-label">Phone</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="prep">+36</span>
                                </div>
                                <input name="phone" type="text" placeholder="" data-mask="(99) 999-9999"
                                    class="form-control">
                            </div>
                        </div>


                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="sex" id="sex" value="1">
                            <label class="form-check-label" for="">Férfi</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="sex" id="sex" value="2">
                            <label class="form-check-label" for="">Nő</label>
                        </div>
                        <div class="form-check mb-3">
                            <input class="form-check-input" type="radio" name="sex" id="sex" value="3">
                            <label class="form-check-label" for="">Other</label>
                        </div>

                        <div class="form-check mb-3">
                            <input class="form-check-input" type="checkbox" name="confirm" id="Confirm">
                            <label class="form-check-label" for="Confirm" required>
                                Confirm you are human.
                            </label>
                        </div>

                        <div class="text-center mb-3">
                            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<!-- Content vége -->

<script>
    /* Parsley magyar lokalizáció */
    Parsley.addMessages('hu', {
        defaultMessage: "Érvénytelen mező.",
        type: {
            email: "Érvénytelen email cím.",
            url: "Érvénytelen URL cím.",
            number: "Érvénytelen szám.",
            integer: "Érvénytelen egész szám.",
            digits: "Érvénytelen szám.",
            alphanum: "Érvénytelen alfanumerikus érték."
        },
        notblank: "Ez a mező nem maradhat üresen.",
        required: "A mező kitöltése kötelező.",
        pattern: "A jelszónak tartalmaznia kell legalább egy kis betűt, egy nagy betűt, számot és speciális karaktert.",
        min: "A mező értéke nagyobb vagy egyenlő kell legyen mint %s.",
        max: "A mező értéke kisebb vagy egyenlő kell legyen mint %s.",
        range: "A mező értéke %s és %s közé kell essen.",
        minlength: "Legalább %s karakter megadása szükséges.",
        maxlength: "Legfeljebb %s karakter megadása engedélyezett.",
        length: "Nem megfelelő karakterszám. Minimum %s, maximum %s karakter adható meg.",
        mincheck: "Legalább %s értéket kell kiválasztani.",
        maxcheck: "Maximum %s értéket lehet kiválasztani.",
        check: "Legalább %s, legfeljebb %s értéket kell kiválasztani.",
        equalto: "A jelszavak nem egyeznek."
    });

    Parsley.setLocale('hu');

    /* Parsley meghívása a regFormra */
    $('#registrationForm').parsley();
</script>

<?php require_once "../layouts/footer.php"; ?>