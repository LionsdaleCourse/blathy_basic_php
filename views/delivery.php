<?php
require_once '../layouts/header.php';
require_once '../controllers/database.php';
?>

<?php
/* TODO: Complete validation rules on inputs after password */
/* TODO: Tömbösítése az address inputnak */

/* Számlához */
$sql_query = "SELECT * FROM nationalities";
$result = mysqli_query($connection, $sql_query);

/* Szállításhoz */
$sql_query2 = "SELECT * FROM nationalities";
$result2 = mysqli_query($connection, $sql_query);
?>

<!-- Content eleje -->
<div class="container mt-5">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="row text-center">
                        <h4 class="card-title">Delivery Info</h4>
                        <p class="card-subtitle">Számlázási adatok</p>
                    </div>

                    <form action="../controllers/order.php" method="POST" id="orderForm">
                        <!-- Method lehet GET vagy POST -->
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" name="email" required value="<?php echo isset($_SESSION['user_email']) ?  $_SESSION['user_email'] : ''; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="FullName" class="form-label">FullName</label>
                            <input type="text" class="form-control" id="FullName" name="fullname[]">
                        </div>
                        <div class="mb-3">
                            <label for="postal" class="form-label">Postal Code</label>
                            <input type="number" class="form-control" id="postal" name="postal[]">
                        </div>
                        <div class="mb-3">
                            <label for="Country" class="form-label">Country</label>
                            <select id="Country" class="form-control" name="country[]">
                                <option value="#" disabled selected>Please choose</option>

                                <?php
                                while ($row = mysqli_fetch_assoc($result)) {
                                    echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="Street" class="form-label">Street</label>
                            <input type="text" class="form-control" id="street" name="street[]">
                        </div>
                        <div class="mb-3 form-group">
                            <label class="form-label">Phone</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="prep">+36</span>
                                </div>
                                <input name="phone" type="text" placeholder="" data-mask="(99) 999-9999"
                                    class="form-control">
                            </div>
                        </div>
                        <!-- FORM SWITCH -->
                        <div class="text-center mb-4">
                            <p for="flexSwitchCheckDefault">Billing === Delivery
                                address</p>
                            <div class="form-check form-switch d-flex justify-content-center">
                                <input checked class="form-check-input" type="checkbox" role="switch"
                                    id="flexSwitchCheckDefault" onchange="otherDelivery()" name="switch">
                            </div>
                        </div>

                        <!-- Different Delivery -->
                        <div id="deliveryinfo" class="d-none">
                            <div class="mb-3">
                                <label for="FullName" class="form-label">FullName</label>
                                <input type="text" class="form-control" id="FullName" name="fullname[]">
                            </div>
                            <div class="mb-3">
                                <label for="postal" class="form-label">Postal Code</label>
                                <input type="number" class="form-control" id="postal" name="postal[]">
                            </div>
                            <div class="mb-3">
                                <label for="Country" class="form-label">Country</label>
                                <select id="Country" class="form-control" name="country[]">
                                    <option value="#" disabled selected>Please choose</option>

                                    <?php
                                    while ($row = mysqli_fetch_assoc($result2)) {
                                        echo '<option value="' . $row["id"] . '">' . $row["name"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="Street" class="form-label">Street</label>
                                <input type="text" class="form-control" id="Street" name="street[]">
                            </div>
                        </div>

                        <div class="text-center mb-3">
                            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<!-- Content vége -->

<script>
    /* Parsley magyar lokalizáció */
    Parsley.addMessages('hu', {
        defaultMessage: "Érvénytelen mező.",
        type: {
            email: "Érvénytelen email cím.",
            url: "Érvénytelen URL cím.",
            number: "Érvénytelen szám.",
            integer: "Érvénytelen egész szám.",
            digits: "Érvénytelen szám.",
            alphanum: "Érvénytelen alfanumerikus érték."
        },
        notblank: "Ez a mező nem maradhat üresen.",
        required: "A mező kitöltése kötelező.",
        pattern: "A jelszónak tartalmaznia kell legalább egy kis betűt, egy nagy betűt, számot és speciális karaktert.",
        min: "A mező értéke nagyobb vagy egyenlő kell legyen mint %s.",
        max: "A mező értéke kisebb vagy egyenlő kell legyen mint %s.",
        range: "A mező értéke %s és %s közé kell essen.",
        minlength: "Legalább %s karakter megadása szükséges.",
        maxlength: "Legfeljebb %s karakter megadása engedélyezett.",
        length: "Nem megfelelő karakterszám. Minimum %s, maximum %s karakter adható meg.",
        mincheck: "Legalább %s értéket kell kiválasztani.",
        maxcheck: "Maximum %s értéket lehet kiválasztani.",
        check: "Legalább %s, legfeljebb %s értéket kell kiválasztani.",
        equalto: "A jelszavak nem egyeznek."
    });

    Parsley.setLocale('hu');

    /* Parsley meghívása a regFormra */
    $('#registrationForm').parsley();
</script>

<!-- Other delivery -->
<script>
    let showDelivery = false;
    let otherDelivery = () => {
        let delivery = document.getElementById("deliveryinfo");

        if (!showDelivery) {
            delivery.classList.remove("d-none");
            showDelivery = true;
        }
        else {
            delivery.classList.add("d-none");
            showDelivery = false;
        }
    }

    function valami () {

    }
</script>

<?php require_once "../layouts/footer.php"; ?>
