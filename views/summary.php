<?php
require_once '../layouts/header.php';
require_once '../controllers/database.php';

/* Üres kosár létrehozása, majd ha van, akkor a Sessionben lévő kosár lekérése */
$cart = array();
if (isset($_SESSION['cart'])) {
    $cart = $_SESSION['cart'];
}
/* Div content létezik */
?>

<div class="card m-5">
    <div class="card-body">
        <h4 class="card-title">Megrendelés összesítő</h4>
        <p class="card-text text-danger">*Jól nézze meg mit rendel!</p>

        <div class="table-responsive">
            <table class="table
        table-hover	
        table-borderless
        align-middle">
                <thead>
                    <caption>Megrendelés összesítő</caption>
                    <tr>
                        <th>Id</th>
                        <th>Név</th>
                        <th>Mennyiség</th>
                        <th>Egységár</th>
                        <th>Összesen</th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">

                    <?php

                    if (count($cart) == 0) {
                        echo '<tr>';
                        echo '<td colspan="4">Nincs semmi a kosárban.<td>';
                        echo '</tr>';
                    } else {
                        $id = 1;
                        $total = 0;
                        foreach ($cart as $key => $value) {
                            echo '<tr>';
                            echo '<td scope="row">'.$id.'</td>';
                            echo '<td>'.$value['name'].'</td>';
                            echo '<td>'.$value['qty'].'</td>';
                            echo '<td>$'.$value['price'].'</td>';
                            echo '<td>$'.number_format(($value['price'] * $value['qty']), 2).'</td>';
                            echo '</tr>';
                            $id++; 
                            $total += $value['price'] * $value['qty'];
                        }
                    }
                    ?>
                    <tr>
                        <td colspan="4" class="text-end" style="font-weight: bold;">Végösszesen:</td>
                        <td> <?php echo isset($total) ? "$".number_format($total, 2) : 0?> </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id ="card-submit-button" class="text-end" style="margin-right: 16%">
            <a href="delivery.php" class="btn btn-success">Tovább a szállításhoz</a>
        </div>
        <?php var_dump($_SESSION['cart']); ?>

    </div>
</div>

<!-- TODO: 
    TODO:
        Csinálni egy tovább a szállítási infokhoz oldalt
        Csinálni egy basic form (Számlázási Név,/cím/adószám/tel ha van, szállítási cím legyen ott, 
        de ha bepipálja hogy ugyanaz mint a számlázási, akkor tűnjön el. (js))
-->

<?php require_once '../layouts/footer.php'; ?>