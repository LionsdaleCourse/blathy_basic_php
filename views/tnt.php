<?php

/* 
    TODO: Fizetés gomb
    TODO: Pagináció a termékekre
*/

require_once '../layouts/header.php';
require_once '../controllers/database.php';

/* Szűrővel lekérdezés */
if (isset($_GET['filter'])) {
    /* Termékek lekérése */
    $filter = $_GET['filter'];
    $query = "SELECT * FROM explosives ORDER BY price $filter";
    $result = mysqli_query($connection, $query);
} else {
    /* Termékek lekérése */
    $query = "SELECT * FROM explosives";
    $result = mysqli_query($connection, $query);
}

?>


<!-- Background video -->
<video playsinline autoplay muted loop poster="#">
    <source src="../assets/videos/c4.mp4" type="video/webm">
    Your browser does not support the video tag.
</video>

<!-- Aloldal címe -->
<div class="row text-center text-white">
    <div class="col mt-4">
        <h4>Használt Robbanóanyagok adás-vétele</h1>
    </div>
</div>

<!-- Szűrő -->
<div class="row m-3">
    <div class="col-6">
        <form action="#" method="GET">
            <div class="row">
                <div class="col-4">
                    <select name="filter" id="filter" class="form-control">
                        <option value="" disabled selected>Válassz...</option>
                        <option value="desc">Sorbarendezés árszerint csökkenő</option>
                        <option value="asc">Sorbarendezés árszerint növekvő</option>
                    </select>
                </div>
                <div class="col-2">
                    <button class="btn btn-danger">Szűrés</button>
                </div>
                <div class="col-2">
                    <form action="" method="GET">
                        <button class="btn btn-warning">Törlés</button>
                    </form>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Lekért adatok -->
<div class="row">
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
        echo '<div class=" col-3">';
        echo '<div class="card border m-3">';
        echo '<img class="card-img-top" src="../assets/images/' . $row['picture'] . '" alt="Title">';
        echo '<div class="card-body">';
        echo '<h4 class="card-title">' . $row['name'] . ' <span class="blockquote-footer" style="font-size: 12px;">' . $row['price'] . '$/' . $row['qty_name'] . '</span></h4>';
        echo '<p class="card-text text-truncate">' . $row['description'] . '</p>';
        echo '</div>';
        echo '<form action="../controllers/cart.php" method="POST">';
        echo '<div class="form-group m-1">';
        echo '<input class="form-control" type="number" min="1" name="quantity" id="quantity" value=1 required>';
        echo '</div>';
        echo '<input type="hidden" name="id" value="' . $row['id'] . '">';
        echo '<div class="form-group text-center m-2">';
        echo '<button type="submit" name="submit" class="cart-icon"><i class="fa-solid fa-cart-plus" style="color: #9f1400;"></i></button>';
        echo '</div>';
        echo '</form>';
        echo '</div>';
        echo '</div>';
    }
    ?>
</div>

<!-- Kosár -->
<div class="row m-5">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-borderless table-danger align-middle">
                <thead class="table-warning text-center">
                    <tr>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">

                    <!-- Kosár PHP kódjai -->
                    <?php

                    /* A végösszesen létrehozása */
                    $total = 0;

                    if (isset($_SESSION['cart'])) {
                        /* Kosár lehívása a sessionből */
                        $cart = $_SESSION['cart'];

                        /* A végösszesen kiiratása és a kosár tartalmának kiiratása a táblázatba */
                        foreach ($cart as $key => $value) {
                            $total += $value['price'] * $value['qty'];

                            echo '<tr class="text-center">
                            <td scope="row">' . $value['name'] . '</td>
                            <td>
                                <form action="../controllers/updateCart.php" method="POST">
                                <div>
                                    <input type="number" name="qty" id="qty" class="form-control" value="' . $value['qty'] . '" >
                                </div>                                
                            </td>
                            <td>' . ($value['price'] * $value['qty']) . '</td>
                            <td style="width: 25%;">
                                <div class="row">
                                    <div class="col">                                        
                                            <button class="btn btn-warning" name="submit">Frissít</button>
                                            <input type="hidden" name="update_id" value="'.$key.'">
                                        </form>
                                    </div>
                                    <div class="col">
                                        <form action="../controllers/deleteCart.php" method="POST">
                                            <button class="btn btn-danger" name="submit">Töröl</button>
                                            <input type="hidden" name="delete_id" value="'.$key.'">
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>';

                        }
                    }
                    ?>
                    

                    <!-- Első sor minta -->

                    <!-- Első sor minta VÉGE -->
                    <tr>
                        <td colspan="3" class="text-end">Total:</td>
                        <td class="text-center">
                            <?php echo '$' . $total . ' ( $' . $total * 0.73 . ' + $' . $total * 0.27 . ' VAT)'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-end"></td>
                        <td class="text-center">
                            <a href="summary.php" class="btn btn-outline-success">Tovább a fizetéshez</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<?php require_once '../layouts/footer.php'; ?>